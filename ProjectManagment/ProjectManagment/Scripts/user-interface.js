﻿function WebApiMethods() {
    var self = this;

    self.Status = ko.observable("Status");

    self.SuccessMethod = null;

    self.postObject = function (url, obj) {
        self.Status("Sending");

        $.ajax({
            url: url,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(obj),
            success: function () {
                if (self.SuccessMethod) {
                    self.SuccessMethod();
                    self.SuccessMethod = null;
                }
                self.Status("OK!");
            }
        }).fail(
            function (xhr, textStatus, err) {
                self.SuccessMethod = null;
                self.Status("Error!");
            });
    };

    self.getObject = function (url, func) {
        self.Status("Sending");

        $.ajax({
            url: url,
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                func(data);
                if (self.SuccessMethod) {
                    self.SuccessMethod();
                    self.SuccessMethod = null;
                }
                self.Status("OK!");
            }
        }).fail(
            function (xhr, textStatus, err) {
                self.SuccessMethod = null;
                self.Status("Error!");
            });
    };

    self.deleteObject = function (url) {
        self.Status("Sending");

        $.ajax({
            url: url,
            type: "DELETE",
            contentType: 'application/json; charset=utf-8',
            success: function () {
                if (self.SuccessMethod) {
                    self.SuccessMethod();
                    self.SuccessMethod = null;
                }
                self.Status("OK!");
            }
        }).fail(
            function (xhr, textStatus, err) {
                self.SuccessMethod = null;
                self.Status("Error!");
            });
    };
}

function ProjectMethods(api) {
    var self = this;

    var someDebugProject = {
        Name: "Some Project",
        Discription: "Very some",
        BeginDate: new Date().toLocaleDateString(),
        EndDate: new Date(2015, 4, 23).toLocaleDateString(),
        Status: "In progres"
    };

    var someDebugWorker = {
        BeginDate: new Date().toLocaleDateString(),
        EndDate: new Date(2017, 4, 23).toLocaleDateString(),
        PositionInProject: "Some Position"
    };


    self.Projects = ko.observableArray([]);
    self.Current = ko.observable({});
    self.CurrentHiddenClass = ko.computed(function (val) {
        if (self.Current().Id != undefined)
            return "";
        else
            return "hidden"
    });

    self.GetAll = function () {
        api.getObject("/api/project", function (data) {
            self.Projects(data);
        });
    };

    self.Add = function () {
        api.postObject("/api/project", someDebugProject);
    }

    self.GetInfo = function (id) {
        api.getObject("/api/project/" + id, function (data) {
            self.Current(data);
        });
    };

    self.Delete = function (id) {
        api.deleteObject("/api/project/" + id);
    };

    self.Join = function (id, id2) {
        api.postObject("/api/project/" + id + "/" + id2, someDebugWorker);
    };

    self.Release = function (id, id2) {
        api.deleteObject("/api/project/" + id + "/" + id2);
    };
}

function PersonMethods(api) {
    var self = this;

    var someDebugPerson = {
        FullName: "Валерий Олександрович",
        Position: "Менеджер"
    };

    self.Persons = ko.observableArray([]);
    self.Current = ko.observable({});
    self.CurrentHiddenClass = ko.computed(function (val) {
        if (self.Current().Id != undefined)
            return "";
        else
            return "hidden"
    });

    self.GetAll = function () {
        api.getObject("/api/person", function (data) {
            self.Persons(data);
        });
    };

    self.Add = function () {
        api.postObject("/api/person", someDebugPerson);
    }

    self.GetInfo = function (id) {
        api.getObject("/api/person/" + id, function (data) {
            self.Current(data);
        });
    };

    self.Delete = function (id) {
        api.deleteObject("/api/person/" + id);
    };
}

function ViewModel() {
    var self = this;

    self.WebApi = new WebApiMethods();
    self.ProjectApi = new ProjectMethods(self.WebApi);
    self.PersonApi = new PersonMethods(self.WebApi);

    self.CurrentProjectSelected = ko.computed(function () {
        return self.ProjectApi.CurrentHiddenClass();
    });

    self.CurrentPersonSelected = ko.computed(function () {
        return self.PersonApi.CurrentHiddenClass();
    });

    ////////////
    self.AddProject = function () {
        self.WebApi.SuccessMethod = function () {
            self.ProjectApi.GetAll();
        }
        self.ProjectApi.Add();
    }

    self.GetProjectInfo = function (project) {
        self.ProjectApi.GetInfo(project.Id);
    }

    self.DeleteProject = function (project) {
        self.WebApi.SuccessMethod = function () {
            self.PersonApi.Current({});
            self.ProjectApi.Current({});
            self.ProjectApi.GetAll();
        }
        self.ProjectApi.Delete(project.Id);
    }

    ////////////
    self.AddPerson = function () {
        self.WebApi.SuccessMethod = function () {
            self.PersonApi.GetAll();
        }
        self.PersonApi.Add();
    }

    self.GetPersonInfo = function (person) {
        self.PersonApi.GetInfo(person.Id);
    }

    self.DeletePerson = function (person) {
        self.WebApi.SuccessMethod = function () {
            self.ProjectApi.Current({});
            self.PersonApi.Current({});
            self.PersonApi.GetAll();
        }
        self.PersonApi.Delete(person.Id);
    }
    ////////////


    self.JoinCurrentProjectPerson = function () {
        var id = self.ProjectApi.Current().Id;
        var id2 = self.PersonApi.Current().Id;

        self.WebApi.SuccessMethod = function () {
            self.ProjectApi.GetInfo(id);
            self.PersonApi.GetInfo(id2);
        }
        self.ProjectApi.Join(id, id2);
    };

    self.ReleaseCurrentProjectPerson = function () {
        var id = self.ProjectApi.Current().Id;
        var id2 = self.PersonApi.Current().Id;

        self.WebApi.SuccessMethod = function () {
            self.ProjectApi.GetInfo(id);
            self.PersonApi.GetInfo(id2);
        }
        self.ProjectApi.Release(id, id2);
    };

    self.ProjectApi.GetAll();
    self.PersonApi.GetAll();
}

$(window).load(function () {
    ko.applyBindings(new ViewModel());
});