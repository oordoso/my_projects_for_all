﻿using ProjectManagment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagment.Engine
{
    public partial class ProjectManagment
    {
        public void Join(Worker projectPerson)
        {
            if (_workers.Count((w) => w.Project.Id == projectPerson.Project.Id &&
                                    w.Person.Id == projectPerson.Person.Id) != 0)
                throw new Exception("Person has already been working in this project");

            var existProject = _projects.First((p) => p.Id == projectPerson.Project.Id);
            var existPerson = _persons.First((p) => p.Id == projectPerson.Person.Id);

            projectPerson.Project = existProject;
            projectPerson.Person = existPerson;

            _workers.Add(projectPerson);
        }

        public void Release(int projectId, int personId)
        {
            if (_workers.Count((w) => w.Project.Id == projectId &&
                                    w.Person.Id == personId) == 0)
                throw new Exception("Person has not been working in this project");

            var existJoin = _workers.First((w) => w.Project.Id == projectId &&
                                    w.Person.Id == personId);
            _workers.Remove(existJoin);
        }
    }
}