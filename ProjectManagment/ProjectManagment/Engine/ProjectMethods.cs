﻿using ProjectManagment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagment.Engine
{
    public partial class ProjectManagment
    {
        public void Add(Project project)
        {
            if (_projects.Count > 0)
                project.Id = _projects.Max((p) => p.Id) + 1;
            _projects.Add(project);
        }

        public void Update(Project project)
        {
            if (_projects.Count((p) => p.Id == project.Id) == 0)
                throw new Exception("None object");

            var existProject = _projects.First((p) => p.Id == project.Id);

            existProject.Name = project.Name;
            existProject.Discription = project.Discription;
            existProject.BeginDate = project.BeginDate;
            existProject.EndDate = project.EndDate;
            existProject.Status = project.Status;
        }

        public void RemoveProject(int id)
        {
            if (_projects.Count((p) => p.Id == id) == 0)
                throw new Exception("None object");

            var existProject = _projects.First((p) => p.Id == id);
            _projects.Remove(existProject);

            _workers.RemoveAll((w) => w.Project.Id == existProject.Id);
        }
    }
}