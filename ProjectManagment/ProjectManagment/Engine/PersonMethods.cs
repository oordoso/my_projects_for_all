﻿using ProjectManagment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagment.Engine
{
    public partial class ProjectManagment
    {
        public void Add(Person person)
        {
            if (_persons.Count > 0)
                person.Id = _persons.Max((p)=>p.Id) + 1;
            _persons.Add(person);
        }

        public void Update(Person person)
        {
            if (_persons.Count((p) => p.Id == person.Id) == 0)
                throw new Exception("None object");

            var existPerson = _persons.First((p) => p.Id == person.Id);

            existPerson.FullName = person.FullName;
            existPerson.Position = person.Position;
        }

        public void RemovePerson(int id)
        {
            if (_persons.Count((p) => p.Id == id) == 0)
                throw new Exception("None object");

            var existPerson = _persons.First((p) => p.Id == id);
            _persons.Remove(existPerson);

            _workers.RemoveAll((w) => w.Person.Id == existPerson.Id);
        }
    }
}