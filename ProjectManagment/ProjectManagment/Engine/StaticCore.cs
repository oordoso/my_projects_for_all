﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagment.Engine
{
    public class StaticCore
    {
        static ProjectManagment _core;
        public static  ProjectManagment Core
        {
            get
            {
                if (_core == null)
                    _core = new ProjectManagment();
                return _core;
            }
        }

    }
}