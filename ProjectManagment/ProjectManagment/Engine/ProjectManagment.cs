﻿using ProjectManagment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagment.Engine
{
    public partial class ProjectManagment
    {
        readonly List<Person> _persons = new List<Person>();
        readonly List<Project> _projects = new List<Project>();
        readonly List<Worker> _workers = new List<Worker>(); //Many-to-many connector for Project and Person

        public ProjectManagment()
        {
            Add(new Person() { FullName = "Василий Иванович", Position = "Менеджер" });
            Add(new Person() { FullName = "Иван Петрович", Position = "Программист" });
            Add(new Person() { FullName = "Евгений Дмитриевич", Position = "Программист" });
            Add(new Person() { FullName = "Петр Сергеевич", Position = "Программист" });
            Add(new Person() { FullName = "Дмитрий Сергеевич", Position = "Тестировщик" });
            Add(new Person() { FullName = "Евгений Иванович", Position = "Тестировщик" });

            Add(new Project() { Name = "Ronobot", Discription = "Проект ниочем.", BeginDate = new DateTime(2013, 3, 14), EndDate = new DateTime(2013, 12, 24), Status = "Finished" });
            Add(new Project() { Name = "FunnyStaff", Discription = "Проект, который не доделаем никогда.", BeginDate = new DateTime(2014, 2, 12), EndDate = DateTime.Now, Status = "In progress" });

            Join(new Worker() { Person = new Person() { Id = 0 }, Project = new Project() { Id = 0 }, BeginDate = new DateTime(2013, 3, 14), EndDate = new DateTime(2013, 12, 24), PositionInProject = "Team Leader" });
            Join(new Worker() { Person = new Person() { Id = 1 }, Project = new Project() { Id = 0 }, BeginDate = new DateTime(2013, 3, 14), EndDate = new DateTime(2013, 7, 21), PositionInProject = "Главный программист" });
            Join(new Worker() { Person = new Person() { Id = 4 }, Project = new Project() { Id = 0 }, BeginDate = new DateTime(2013, 4, 15), EndDate = new DateTime(2013, 5, 23), PositionInProject = "QA" });
            Join(new Worker() { Person = new Person() { Id = 0 }, Project = new Project() { Id = 1 }, BeginDate = new DateTime(2014, 3, 14), EndDate = new DateTime(2014, 11, 23), PositionInProject = "Менеджер" });
            Join(new Worker() { Person = new Person() { Id = 2 }, Project = new Project() { Id = 1 }, BeginDate = new DateTime(2014, 2, 14), EndDate = new DateTime(2014, 12, 4), PositionInProject = "Team Leader" });
            Join(new Worker() { Person = new Person() { Id = 5 }, Project = new Project() { Id = 1 }, BeginDate = new DateTime(2014, 2, 14), EndDate = new DateTime(2014, 11, 21), PositionInProject = "QA" });
            Join(new Worker() { Person = new Person() { Id = 3 }, Project = new Project() { Id = 1 }, BeginDate = new DateTime(2014, 3, 15), EndDate = new DateTime(2014, 12, 24), PositionInProject = "Программист" });
        }
    }
}