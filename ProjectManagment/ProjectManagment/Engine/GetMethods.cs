﻿using ProjectManagment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagment.Engine
{
    public partial class ProjectManagment
    {
        public Project[] GetAllProjects()
        {
            return _projects.ToArray();
        }

        public Project GetProject(int id)
        {
            if (_projects.Count((p) => p.Id == id) == 0)
                return null;

            var project = _projects.First((p) => p.Id == id)
                .CleanCopy();//to avoid cicles
            var workers = _workers
                .Where((w) => w.Project.Id == id)
                .Select((w)=>w.CleanCopy(w.Person)) //to avoid cicles
                .ToArray();
            project.Persons = workers;

            return project;
        }

        public Person[] GetAllPersons()
        {
            return _persons.ToArray();
        }

        public Person GetPerson(int id)
        {
            if (_persons.Count((p) => p.Id == id) == 0)
                return null;

            var person = _persons.First((p) => p.Id == id)
                .CleanCopy();//to avoid cicles
            var workers = _workers
                .Where((w) => w.Person.Id == id)
                .Select((w) => w.ClearCopy(w.Project))//to avoid cicles
                .ToArray();
            person.Projects = workers;

            return person;
        }

        public Person GetPersonInProject(int projectId, int personId)
        {
            if (_workers.Count((w) => w.Project.Id == projectId &&
                                    w.Person.Id == personId) == 0)
                return null;

            var person = _persons.First((p) => p.Id == personId)
                .CleanCopy();//to avoid cicles
            var workers = _workers
                .Where((w) => w.Project.Id == projectId &&
                              w.Person.Id == personId)
                .Select((w) => w.ClearCopy(w.Project))//to avoid cicles
                .ToArray();
            person.Projects = workers;

            return person;
        }
    }
}