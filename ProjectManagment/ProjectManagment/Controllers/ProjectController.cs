﻿using ProjectManagment.Engine;
using ProjectManagment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectManagment.Controllers
{
    public class ProjectController : ApiController
    {
        public IEnumerable<Project> Get()
        {
            return StaticCore.Core.GetAllProjects();
        }

        public Project Get(int id)
        {
            return StaticCore.Core.GetProject(id);
        }

        public Person Get(int id, int id2)
        {
            return StaticCore.Core.GetPersonInProject(id,id2);
        }

        public void Post(int id, int id2, Worker worker)
        {
            worker.Project = new Project(){Id=id};
            worker.Person = new Person(){Id=id2};
            StaticCore.Core.Join(worker);
        }

        public void Post(Project pr)
        {
            StaticCore.Core.Add(pr);
        }

        public void Put(int id, Project pr)
        {
            pr.Id = id;
            StaticCore.Core.Update(pr);
        }

        public void Delete(int id)
        {
            StaticCore.Core.RemoveProject(id);
        }

        public void Delete(int id, int id2)
        {
            StaticCore.Core.Release(id, id2);
        }
    }
}
