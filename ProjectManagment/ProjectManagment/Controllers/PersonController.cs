﻿using ProjectManagment.Engine;
using ProjectManagment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectManagment.Controllers
{
    public class PersonController : ApiController
    {
        public IEnumerable<Person> Get()
        {
            return StaticCore.Core.GetAllPersons();
        }

        public Person Get(int id)
        {
            return StaticCore.Core.GetPerson(id);
        }

        public void Post(Person pr)
        {
            StaticCore.Core.Add(pr);
        }

        public void Put(int id, Person pr)
        {
            pr.Id = id;
            StaticCore.Core.Update(pr);
        }

        public void Delete(int id)
        {
            StaticCore.Core.RemovePerson(id);
        }
    }
}
