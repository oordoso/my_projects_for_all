﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ProjectManagment
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}/{id2}",
                defaults: new { id = RouteParameter.Optional, id2 = RouteParameter.Optional}
            );
        }
    }
}
