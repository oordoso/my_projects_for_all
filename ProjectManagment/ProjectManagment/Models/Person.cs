﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagment.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Position { get; set; }

        public Worker[] Projects { get; set; }

        public Person CleanCopy()
        {
            return new Person()
            {
                Id = Id,
                FullName = FullName,
                Position = Position,
            };
        }
    }
}