﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagment.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }

        public Worker[] Persons { get; set; }

        public Project CleanCopy()
        {
            return new Project()
            {
                Id = Id,
                Name = Name,
                Discription = Discription,
                BeginDate = BeginDate,
                EndDate = EndDate,
                Status = Status
            };
        }
    }
}