﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagment.Models
{
    //Many-to-many connector for Project and Person
    public class Worker
    {
        public Project Project { get; set; }
        public Person Person { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PositionInProject { get; set; }

        public Worker CleanCopy(Person person)
        {
            return new Worker()
            {
                Person = person,
                BeginDate = BeginDate,
                EndDate = EndDate,
                PositionInProject = PositionInProject
            };
        }

        public Worker ClearCopy(Project project)
        {
            return new Worker()
            {
                Project = project,
                BeginDate = BeginDate,
                EndDate = EndDate,
                PositionInProject = PositionInProject
            };
        }
    }
}