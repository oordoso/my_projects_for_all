﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sapper.JsonModels
{
    public class UserActionModel
    {
        public enum ActionEnum
        {
            OpenCell,
            OpenCellAround,
            ToggleFlag,
            Invalid
        }

        [JsonIgnore] public static readonly Dictionary<string, ActionEnum> ActionNames = new Dictionary
            <string, ActionEnum>
        {
            {"open", ActionEnum.OpenCell},
            {"around", ActionEnum.OpenCellAround},
            {"flag", ActionEnum.ToggleFlag},
        };

        private string _actionName;

        [JsonIgnore]
        public ActionEnum Action { get; private set; }

        public string ActionName
        {
            get { return _actionName; }
            set
            {
                _actionName = value;
                if (ActionNames.ContainsKey(ActionName))
                    Action = ActionNames[_actionName];
                else
                    Action = ActionEnum.Invalid;
            }
        }

        public int Row { get; set; }
        public int Column { get; set; }
    }
}