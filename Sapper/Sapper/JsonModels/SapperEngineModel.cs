﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Sapper.SapperEngine;

namespace Sapper.JsonModels
{
    public class SapperEngineModel
    {
        [JsonIgnore] public static readonly Dictionary<SapperModelCellType, string> CellTypeNames = new Dictionary
            <SapperModelCellType, string>
        {
            {SapperModelCellType.Flag, "l"},
            {SapperModelCellType.Hide, "h"},
            {SapperModelCellType.Mine, "m"},
            {SapperModelCellType.Free, "f"},
            {SapperModelCellType.None, "n"}
        };

        [JsonIgnore] public static readonly Dictionary<GameState, string> GameStateNames = new Dictionary
            <GameState, string>
        {
            {GameState.InGame, "ingame"},
            {GameState.Victory, "victory"},
            {GameState.GameOver, "gameover"},
        };

        public string[][] Matrix { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int MinesNumber { get; set; }
        public int AvaliableFlagsNumber { get; set; }
        public string State { get; set; }
        public string EndTime { get; set; }
    }
}