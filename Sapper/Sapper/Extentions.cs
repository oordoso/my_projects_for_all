﻿using System;
using Sapper.JsonModels;
using Sapper.SapperEngine;

namespace Sapper
{
    public static class Extentions
    {
        public static SapperEngineModel GetModel(this SapperEngine.SapperEngine self)
        {
            var matrix = new string[self.Height][];

            for (int pi = 0; pi < self.Height; pi++)
            {
                matrix[pi] = new string[self.Width];
                for (int pj = 0; pj < self.Width; pj++)
                {
                    if (self.Matrix[pj, pi] == null)
                    {
                        matrix[pi][pj] = SapperEngineModel.CellTypeNames[SapperModelCellType.None];
                        continue;
                    }

                    if (self.Matrix[pj, pi].IsFlag)
                    {
                        matrix[pi][pj] = SapperEngineModel.CellTypeNames[SapperModelCellType.Flag];
                        continue;
                    }

                    if (self.Matrix[pj, pi].IsHide)
                    {
                        matrix[pi][pj] = SapperEngineModel.CellTypeNames[SapperModelCellType.Hide];
                        continue;
                    }

                    if (self.Matrix[pj, pi].Type == CellType.Mine)
                    {
                        matrix[pi][pj] = SapperEngineModel.CellTypeNames[SapperModelCellType.Mine];
                        continue;
                    }

                    if (self.Matrix[pj, pi].Type == CellType.NearMine)
                    {
                        matrix[pi][pj] = self.Matrix[pj, pi].Estimate.ToString();
                        continue;
                    }

                    if (self.Matrix[pj, pi].Type == CellType.Free)
                    {
                        matrix[pi][pj] = SapperEngineModel.CellTypeNames[SapperModelCellType.Free];
                    }
                }
            }

            var model = new SapperEngineModel
            {
                Matrix = matrix,
                Width = self.Width,
                Height = self.Height,
                MinesNumber = self.MinesNumber,
                AvaliableFlagsNumber = self.AvaliableFlagsNumber,
                State = SapperEngineModel.GameStateNames[self.GameState],
                EndTime = ConvertDateTimeToJavaScriptDateTicks(self.EndTime).ToString()
            };

            return model;
        }

        private static long ConvertDateTimeToJavaScriptDateTicks(DateTime dt)
        {
            return (dt.Ticks - new DateTime(1970, 1, 1, 3, 0, 0).Ticks)/10000;
        }
    }
}