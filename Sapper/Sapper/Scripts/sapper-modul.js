﻿$(window).load(function() {

    var cellTypes = {
        flag: "l",
        hide: "h",
        mine: "m",
        free: "f",
        near: "e",
        none: "n"
    };

    var cellTypeAttr = {};
    cellTypeAttr[cellTypes.flag] = "attr-flag";
    cellTypeAttr[cellTypes.hide] = "attr-hide";
    cellTypeAttr[cellTypes.mine] = "attr-mine";
    cellTypeAttr[cellTypes.free] = "attr-free";
    cellTypeAttr[cellTypes.near] = "attr-near";
    cellTypeAttr[cellTypes.none] = "attr-none";

    var actions = {
        openCell: "open",
        openCellAround: "around",
        toggleFlag: "flag",
        endGame: "end",
    };

    var gameStates = {
        inGame: "ingame",
        gameOver: "gameover",
        victory: "victory",
    };

    var gameStatesAttr = {};
    gameStatesAttr[gameStates.inGame] = "ingame";
    gameStatesAttr[gameStates.gameOver] = "gameover";
    gameStatesAttr[gameStates.victory] = "victory";

    var gameMessages = {};
    gameMessages[gameStates.inGame] = "You have {0} flags";
    gameMessages[gameStates.gameOver] = "You lose...";
    gameMessages[gameStates.victory] = "You win!!!";

    gameMessages["timelimit"] = "Remaining time: {0}.";

    var endTime = null;
    var timer = null;

    //Helpers------------------------------------------------------
    var getTimeDifference = function(earlierDate, laterDate) {
        var oDiff = {};

        var nTotalDiff = Math.max(laterDate.getTime() - earlierDate.getTime(), 0);

        var msec = nTotalDiff;
        oDiff.total = nTotalDiff;
        oDiff.h = Math.floor(msec / 1000 / 60 / 60);
        msec -= oDiff.h * 1000 * 60 * 60;
        oDiff.m = Math.floor(msec / 1000 / 60);
        msec -= oDiff.m * 1000 * 60;
        oDiff.s = Math.floor(msec / 1000);
        msec -= oDiff.s * 1000;
        oDiff.ms = msec;

        return oDiff;
    };
    var stringFormat = function() {
        var s = arguments[0];
        for (var i = 0; i < arguments.length - 1; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, arguments[i + 1]);
        }

        return s;
    };
    var convertTimeDifferenceToString = function(timeDiff) {
        if (timeDiff.h > 10000)
            return "unlimited";

        var m = timeDiff.m;
        var s = timeDiff.s;
        if (m < 10)
            m = "0" + m;
        if (s < 10)
            s = "0" + s;
        return stringFormat("{0}:{1}:{2}", timeDiff.h, m, s);
    }; //-------------------------------------------------------------


    //Ajax-Requests------------------------------------------------
    var getGameState = function(successHandler) {
        $.ajax({
            type: "POST",
            url: "/Game/GameState",
            success: function(resp) {
                successHandler(resp);
            },
            dataType: "json"
        });
    };

    var sendUserAction = function(sendedObject, successHandler) {
        $.ajax({
            type: "POST",
            url: "/Game/UserAction",
            success: function(resp) {
                successHandler(resp);
            },
            data: sendedObject,
            dataType: "json"
        });
    };
    //------------------------------------------------------------


    //Handlers----------------------------------------------------
    var serverModelHandler = function(data) {
        $(".sapper-area").html("");

        buildMineField(data, $(".sapper-area"));

        $(".sapper-message")
            .removeAttr(gameStates.inGame)
            .attr(gameStatesAttr[data.State], 0)
            .text(stringFormat(gameMessages[data.State], data.AvaliableFlagsNumber));

        if (data.State == gameStates.gameOver || data.State == gameStates.victory)
            clearInterval(timer);

        endTime = new Date(+data.EndTime);
    };

    var timerHandler = function() {
        var currentTime = new Date();
        var timeDiff = getTimeDifference(currentTime, endTime);

        if (timeDiff.total == 0) {
            clearInterval(timer);
            getGameState(serverModelHandler);
        }

        var timeString = convertTimeDifferenceToString(timeDiff);

        $(".sapper-time").text(stringFormat(gameMessages["timelimit"], timeString));
    };

    var cellClickHandler = function(event) {
        var self = $(this);
        var act = actions.openCell;

        if (event.ctrlKey)
            act = actions.toggleFlag;

        if (event.altKey)
            act = actions.openCellAround;

        var action = {
            ActionName: act,
            Row: self.attr("position-row"),
            Column: self.attr("position-column")
        };

        sendUserAction(action, serverModelHandler);
    };
    //-----------------------------------------------------------


    //Sapper-Builder---------------------------------------------
    var buildMineField = function(serverModel, parent) {
        var matrix = serverModel.Matrix;
        var width = serverModel.Width;
        var height = serverModel.Height;

        var table = $("<table>");
        for (var i = 0; i < height; i++) {
            var row = $("<tr>");

            for (var j = 0; j < width; j++) {
                var cell = $("<td>")
                    .attr("class", "sapper-cell")
                    .attr("position-row", i)
                    .attr("position-column", j);

                if (cellTypeAttr[matrix[i][j]] != null)
                    cell.attr(cellTypeAttr[matrix[i][j]], "0");
                else {
                    cell.attr(cellTypeAttr[cellTypes.near], matrix[i][j]);
                    cell.text(matrix[i][j]);
                }

                cell.click(cellClickHandler);

                row.append(cell);
            }

            table.append(row);
        }

        parent.append(table);
    };

    var init = function() {
        getGameState(serverModelHandler);
        timer = setInterval(timerHandler, 100);
    };

    init();
});