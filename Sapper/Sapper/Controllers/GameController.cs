﻿using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using Sapper.JsonModels;
using Sapper.Models;

namespace Sapper.Controllers
{
    public class GameController : Controller
    {
        private SapperEngine.SapperEngine Game
        {
            get { return (SapperEngine.SapperEngine) Session["game"]; }
            set { Session.Add("game", value); }
        }

        private GameSettings Settings
        {
            get { return (GameSettings) Session["settings"]; }
            set { Session.Add("settings", value); }
        }


        //
        // GET: /Game/

        public ActionResult Index()
        {
            if (Game == null)
                return RedirectToAction("Create");

            return View();
        }

        //
        // GET: /Game/Create

        public ActionResult Create()
        {
            return View(new GameSettings());
        }

        public ActionResult Reload()
        {
            if (Settings == null)
                return RedirectToAction("Create");

            var settingEndTime = new DateTime(2050, 1, 1);
            if (Settings.IsTimeLimit)
                settingEndTime = DateTime.Now + Settings.TimeLimit;

            Game = new SapperEngine.SapperEngine(
                Settings.Width,
                Settings.Height,
                Settings.MineNumber,
                settingEndTime,
                Settings.SelectedFigure);

            return RedirectToAction("Index");
        }

        //
        // POST: /Game/Create

        [HttpPost]
        public ActionResult Create(GameSettings settings)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(settings);

                Settings = settings;

                return RedirectToAction("Reload");
            }
            catch
            {
                return View(settings);
            }
        }

        public ActionResult Close()
        {
            Game = null;
            return RedirectToAction("Create");
        }

        [HttpPost]
        public void UserAction(UserActionModel userAction)
        {
            if (Game.State != SapperEngine.GameState.InGame)
            {
                GameState();
                return;
            }

            if (userAction.Action == UserActionModel.ActionEnum.OpenCell)
                Game.OpenCell(userAction.Column, userAction.Row);

            if (userAction.Action == UserActionModel.ActionEnum.OpenCellAround)
                Game.OpenCellsAround(userAction.Column, userAction.Row);

            if (userAction.Action == UserActionModel.ActionEnum.ToggleFlag)
                Game.ToggleFlag(userAction.Column, userAction.Row);

            GameState();
        }

        [HttpPost]
        public void GameState()
        {
            if (Game.State == SapperEngine.GameState.GameOver)
                Game.OpenAllMines();

            SapperEngineModel model = Game.GetModel();
            string jsonObject = JsonConvert.SerializeObject(model);
            Response.Write(jsonObject);
        }
    }
}