﻿using System;

namespace Sapper.SapperEngine.MaskedFigure
{
    internal class Line
    {
        private const double _eps = 0.00000001;
        private readonly double _b;
        private readonly double _k;

        private readonly double _x1;
        private readonly double _x2;
        private double _y1;
        private double _y2;

        public Line(double x1, double y1, double x2, double y2)
        {
            _x1 = x1 + _eps;
            _y1 = y1;
            _x2 = x2;
            _y2 = y2;

            _k = (_y2 - _y1)/(_x2 - _x1);
            _b = _y1 - _k*_x1;
        }

        public bool IsRayCollision(double x, double y)
        {
            var ray = new Line(x, y, 9999.0, 5555.0); //random ray

            //k'x+b'=kx+b
            //(k'-k)x=b-b'
            //x=(b-b')/(k'-k)
            double collX = (_b - ray._b)/(ray._k - _k);

            return Math.Min(_x1, _x2) < collX && collX < Math.Max(_x1, _x2) && x < collX;
        }
    }
}