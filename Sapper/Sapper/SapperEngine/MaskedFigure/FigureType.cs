﻿namespace Sapper.SapperEngine.MaskedFigure
{
    public enum FigureType
    {
        Quadro,
        Romb,
        Triangle,
        Star
    }
}