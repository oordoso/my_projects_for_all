﻿namespace Sapper.SapperEngine
{
    public enum CellType
    {
        Mine,
        NearMine,
        Free,
        None
    }
}