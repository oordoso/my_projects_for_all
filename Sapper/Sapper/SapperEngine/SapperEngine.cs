﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sapper.SapperEngine.MaskedFigure;

namespace Sapper.SapperEngine
{
    public partial class SapperEngine
    {
        private readonly int _height;
        private readonly Mask _mask;
        private readonly Cell[,] _matrix;
        private readonly int _width;
        private int _avaliableFlagsNumber;
        private DateTime _endTime;
        private GameState _gameState;
        private int _minesNumber;

        public SapperEngine(int width, int height, int minesNumber, DateTime endTime,
            FigureType figure = FigureType.Romb)
        {
            _minesNumber = minesNumber;
            _avaliableFlagsNumber = minesNumber;
            _width = width;
            _height = height;
            _matrix = new Cell[_width, _height];
            _gameState = GameState.InGame;
            _endTime = endTime;
            _mask = new Mask(figure, width, height);
            Generate();
        }

        //User Action
        public void OpenCell(int column, int row)
        {
            if (!IsInMatrix(column, row))
                return;

            if (_matrix[column, row].Type == CellType.Free)
                OpenFreeCells(column, row);

            _matrix[column, row].IsHide = false;

            if (_matrix[column, row].Type == CellType.Mine)
            {
                _gameState = GameState.GameOver;
                return;
            }

            if (!IsVictory()) return;
            _gameState = GameState.Victory;
        }

        //User Action
        public void OpenCellsAround(int column, int row)
        {
            for (int pi = row - 1; pi <= row + 1; pi++) //*j - column, *i - row
                for (int pj = column - 1; pj <= column + 1; pj++)
                    if (IsInMatrix(pj, pi) && !_matrix[pj, pi].IsFlag)
                        OpenCell(pj, pi);
        }

        //User Action
        public void ToggleFlag(int column, int row)
        {
            if (!IsInMatrix(column, row) || !_matrix[column, row].IsHide)
                return;

            if (_matrix[column, row].IsFlag)
            {
                _matrix[column, row].IsFlag = false;
                _avaliableFlagsNumber++;
            }
            else
            {
                if (_avaliableFlagsNumber > 0)
                {
                    _matrix[column, row].IsFlag = true;
                    _avaliableFlagsNumber--;
                }
            }
        }

        //When game over
        public void OpenAllMines()
        {
            for (int pi = 0; pi < _height; pi++) //*j - column, *i - row
                for (int pj = 0; pj < _width; pj++)
                    if (_matrix[pj, pi] != null && _matrix[pj, pi].Type == CellType.Mine)
                        _matrix[pj, pi].IsHide = false;
        }

        //It is sended to user


        //Check out of range
        private bool IsInMatrix(int column, int row)
        {
            return 0 <= column && column < _width &&
                   0 <= row && row < _height &&
                   _matrix[column, row] != null;
        }

        //When click in free cell
        private void OpenFreeCells(int column, int row)
        {
            if (!IsInMatrix(column, row) || _matrix[column, row].Type != CellType.Free)
                return;

            //BFS-------------------------------------
            var listOpenedPoints = new[] {new {Column = column, Row = row}}.ToList();

            while (listOpenedPoints.Count > 0)
            {
                //*j - column, *i - row
                int cj = listOpenedPoints[0].Column;
                int ci = listOpenedPoints[0].Row;

                _matrix[cj, ci].IsHide = false;

                listOpenedPoints.RemoveAll(el => !_matrix[el.Column, el.Row].IsHide);

                for (int pi = ci - 1; pi <= ci + 1; pi++)
                    for (int pj = cj - 1; pj <= cj + 1; pj++)
                        if (IsInMatrix(pj, pi))
                        {
                            if (_matrix[pj, pi].Type == CellType.Free)
                            {
                                if (_matrix[pj, pi].IsHide)
                                    listOpenedPoints.Add(new {Column = pj, Row = pi});
                            }
                            else
                                _matrix[pj, pi].IsHide = false;
                        }
            }
            //----------------------------------------
        }

        //Check victory conditions
        private bool IsVictory()
        {
            for (int pi = 0; pi < _height; pi++) //*j - column, *i - row
                for (int pj = 0; pj < _width; pj++)
                    if (_matrix[pj, pi] != null &&
                        _matrix[pj, pi].IsHide &&
                        _matrix[pj, pi].Type != CellType.Mine)
                        return false;
            return true;
        }

        //Randomize new game matrix
        private void Generate()
        {
            var rand = new Random(DateTime.Now.Millisecond);
            var avaliablePoints = new List<int[]>();
            var minePoints = new List<int[]>();

            for (int i = 0; i < _height; i++) //*j - column, *i - row
                for (int j = 0; j < _width; j++)
                    if (_mask.IsPointInFigure(j, i))
                    {
                        _matrix[j, i] = new Cell();
                        _matrix[j, i].IsHide = true;
                        _matrix[j, i].IsFlag = false;
                        _matrix[j, i].Estimate = 0;
                        _matrix[j, i].Type = CellType.Free;

                        avaliablePoints.Add(new[] {j, i});
                    }

            _minesNumber = Math.Min(avaliablePoints.Count, _minesNumber);

            for (int m = 0; m < _minesNumber; m++)
            {
                int aPIndex = rand.Next(avaliablePoints.Count);

                int j = avaliablePoints[aPIndex][0];
                int i = avaliablePoints[aPIndex][1];

                _matrix[j, i].Type = CellType.Mine; //*j - column, *i - row

                minePoints.Add(avaliablePoints[aPIndex]);
                avaliablePoints.RemoveAt(aPIndex);
            }

            for (int m = 0; m < _minesNumber; m++)
            {
                int j = minePoints[m][0];
                int i = minePoints[m][1];

                for (int pi = i - 1; pi <= i + 1; pi++) //*j - column, *i - row
                    for (int pj = j - 1; pj <= j + 1; pj++)
                        if (IsInMatrix(pj, pi) && _matrix[pj, pi].Type != CellType.Mine)
                        {
                            _matrix[pj, pi].Type = CellType.NearMine;
                            _matrix[pj, pi].Estimate++;
                        }
            }
        }
    }
}