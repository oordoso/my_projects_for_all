﻿namespace Sapper.SapperEngine
{
    public enum GameState
    {
        InGame,
        Victory,
        GameOver
    }
}