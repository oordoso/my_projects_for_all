﻿namespace Sapper.SapperEngine
{
    public class Cell
    {
        public CellType Type { get; set; }

        public bool IsHide { get; set; }

        public bool IsFlag { get; set; }

        public int Estimate { get; set; }
    }
}