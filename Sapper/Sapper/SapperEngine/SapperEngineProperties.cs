﻿using System;

namespace Sapper.SapperEngine
{
    public partial class SapperEngine
    {
        public GameState State
        {
            get
            {
                if (_endTime.Ticks - DateTime.Now.Ticks < 0)
                    _gameState = GameState.GameOver;

                return _gameState;
            }
        }

        public Cell[,] Matrix
        {
            get { return _matrix; }
        }

        public int Width
        {
            get { return _width; }
        }

        public int Height
        {
            get { return _height; }
        }

        public int MinesNumber
        {
            get { return _minesNumber; }
        }

        public int AvaliableFlagsNumber
        {
            get { return _avaliableFlagsNumber; }
        }

        public GameState GameState
        {
            get { return _gameState; }
        }

        public DateTime EndTime
        {
            get { return _endTime; }
        }
    }
}