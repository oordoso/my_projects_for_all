﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Sapper.SapperEngine.MaskedFigure;

namespace Sapper.Models
{
    public class GameSettings : IValidatableObject
    {
        private readonly SelectListItem[] _figuresName =
        {
            new SelectListItem
            {
                Value = ((int) FigureType.Quadro).ToString(),
                Text = "Quadro"
            },
            new SelectListItem
            {
                Value = ((int) FigureType.Romb).ToString(),
                Text = "Romb"
            },
            new SelectListItem
            {
                Value = ((int) FigureType.Star).ToString(),
                Text = "Star"
            },
            new SelectListItem
            {
                Value = ((int) FigureType.Triangle).ToString(),
                Text = "Triangle"
            }
        };

        private const int _maxHeigth = 50;
        private const int _maxWidth = 50;

        public int Width { get; set; }

        public int Height { get; set; }

        public int MineNumber { get; set; }

        public bool IsTimeLimit
        {
            get { return TimeLimit.Ticks > 0; }
        }

        public TimeSpan TimeLimit { get; set; }

        public FigureType SelectedFigure { get; set; }

        public IEnumerable<SelectListItem> FigureList
        {
            get { return _figuresName; }
        }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            bool isError = false;

            try
            {
                int w = Width;
                int h = Height;
                int m = MineNumber;

                if (w < 3 || w > _maxWidth)
                    isError = true;

                if (h < 3 || h > _maxHeigth)
                    isError = true;

                if (m < 1 || m > w*h)
                    isError = true;
            }
            catch
            {
                isError = true;
            }

            if (isError)
                yield return new ValidationResult("Invalid settings!", new[] {"Error"});
        }
    }
}