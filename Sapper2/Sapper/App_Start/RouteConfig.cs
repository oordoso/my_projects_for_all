﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Sapper
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Default", "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional}
                );

            routes.MapRoute("About", "{controller}/{action}/{id}",
                new {controller = "Home", action = "About", id = UrlParameter.Optional}
                );

            routes.MapRoute("GameIndex", "{controller}/{action}/{id}",
                new {controller = "Game", action = "Index", id = UrlParameter.Optional}
                );

            routes.MapRoute("GameCreate", "{controller}/{action}/{id}",
                new {controller = "Game", action = "Create", id = UrlParameter.Optional}
                );

            routes.MapRoute("GameReload", "{controller}/{action}/{id}",
                new {controller = "Game", action = "Reload", id = UrlParameter.Optional}
                );

            routes.MapRoute("GameClose", "{controller}/{action}/{id}",
                new {controller = "Game", action = "Close", id = UrlParameter.Optional}
                );

            routes.MapRoute("GameGetGameState", "{controller}/{action}/{id}",
                new {controller = "Game", action = "GetGameState", id = UrlParameter.Optional}
                );

            routes.MapRoute("GameUserAction", "{controller}/{action}/{id}",
                new {controller = "Game", action = "UserAction", id = UrlParameter.Optional}
                );
        }
    }
}