﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Sapper.SapperEngine.MaskedFigure
{
    internal class Figure
    {
        public static readonly Dictionary<FigureType, Figure> Figures = new Dictionary<FigureType, Figure>
        {
            {
                FigureType.Quadro,
                new Figure(
                    new PointF(0, 0),
                    new PointF(1, 0),
                    new PointF(1, 1),
                    new PointF(0, 1))
            },
            {
                FigureType.Romb,
                new Figure(
                    new PointF(0.5f, 0),
                    new PointF(1, 0.5f),
                    new PointF(0.5f, 1),
                    new PointF(0, 0.5f))
            },
            {
                FigureType.Triangle,
                new Figure(
                    new PointF(0.5f, 0),
                    new PointF(1, 1),
                    new PointF(0, 1))
            },
            {
                FigureType.Star,
                new Figure(
                    new PointF(0.5f, 0),
                    new PointF(0.615f, 0.382f),
                    new PointF(1, 0.382f),
                    new PointF(0.687f, 0.62f),
                    new PointF(0.805f, 1),
                    new PointF(0.5f, 0.764f),
                    new PointF(0.19f, 1f),
                    new PointF(0.308f, 0.62f),
                    new PointF(0f, 0.382f),
                    new PointF(0.38f, 0.382f))
            }
        };

        private readonly Line[] _lines;

        public Figure(params PointF[] points)
        {
            if (points.Length < 3)
                throw new Exception("Figure: points count error");

            _lines = new Line[points.Length];
            int last = points.Length - 1;
            for (int i = 0; i < points.Length - 1; i++)
                _lines[i] = new Line(points[i].X, points[i].Y, points[i + 1].X, points[i + 1].Y);
            _lines[last] = new Line(points[last].X, points[last].Y, points[0].X, points[0].Y);
        }

        public bool IsPointInFigure(double x, double y)
        {
            int collCount = 0;
            foreach (Line line in _lines)
                if (line.IsRayCollision(x, y))
                    collCount++;

            return collCount%2 == 1;
        }
    }
}