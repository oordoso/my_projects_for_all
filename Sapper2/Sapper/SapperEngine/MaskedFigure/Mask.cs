﻿namespace Sapper.SapperEngine.MaskedFigure
{
    public class Mask
    {
        private readonly Figure _figure;
        private readonly int _height;
        private readonly int _width;

        public Mask(FigureType type, int width, int height)
        {
            _figure = Figure.Figures[type];
            _width = width;
            _height = height;
        }

        public bool IsPointInFigure(int j, int i)
        {
            double w = _width;
            double h = _height;
            double dw = 1/w;
            double dh = 1/h;

            double x = j/w + dw/2;
            double y = i/h + dh/2;

            return _figure.IsPointInFigure(x, y);
        }
    }
}