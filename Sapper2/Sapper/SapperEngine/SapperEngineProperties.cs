﻿using System;

namespace Sapper.SapperEngine
{
    public partial class SapperEngine
    {
        public bool[,] Matrix
        {
            get { return _matrix; }
        }

        public int Width
        {
            get { return _width; }
        }

        public int Height
        {
            get { return _height; }
        }

        public int MinesNumber
        {
            get { return _minesNumber; }
        }

        public int AvaliableFlagsNumber
        {
            get { return _avaliableFlagsNumber; }
        }

        public TimeSpan GameTime
        {
            get { return _gameTime; }
        }

        public DateTime StartTime
        {
            get { return _starTime; }
        }
        
        public TimeSpan ElapsedTime
        {
            get { return _endTime - _starTime; }
        }
    }
}