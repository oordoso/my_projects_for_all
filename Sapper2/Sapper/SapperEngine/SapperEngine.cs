﻿using System;
using Sapper.Models;
using Sapper.SapperEngine.MaskedFigure;

namespace Sapper.SapperEngine
{
    public partial class SapperEngine
    {
        private readonly int _avaliableFlagsNumber;
        private readonly TimeSpan _gameTime;
        private readonly int _height;
        private readonly Mask _mask;
        private readonly bool[,] _matrix;
        private readonly int _minesNumber;
        private readonly int _width;
        private DateTime _endTime;
        private DateTime _starTime;

        public SapperEngine(int width, int height, int minesNumber, TimeSpan gameTime,
            FigureType figure = FigureType.Quadro)
        {
            _minesNumber = minesNumber;
            _avaliableFlagsNumber = minesNumber;
            _width = width;
            _height = height;
            _matrix = new bool[_width, _height];
            _gameTime = gameTime;
            _mask = new Mask(figure, width, height);
            Generate();
        }

        private void Generate()
        {
            for (int i = 0; i < _height; i++) //*j - column, *i - row
                for (int j = 0; j < _width; j++)
                    _matrix[j, i] = _mask.IsPointInFigure(j, i);
        }

        public void StartGame()
        {
            _starTime = DateTime.Now;
        }

        public void StopGame()
        {
            _endTime = DateTime.Now;
        }

        public bool IsValid()
        {
            return _gameTime >= ElapsedTime - new TimeSpan(0, 0, 0, 5);
        }

        public double Estimate(CheckoutData[][] resolvedGameMatrix)
        {
            double estimate = 0;
            for (int i = 0; i < resolvedGameMatrix.Length; i++)
                for (int j = 0; j < resolvedGameMatrix[i].Length; j++)
                    estimate += resolvedGameMatrix[i][j].Estimate();

            estimate *= 100000;
            estimate /= ElapsedTime.TotalMilliseconds;

            return estimate;
        }
    }
}