﻿using System;
using System.Web.Security;
using Sapper.JsonModels;
using Sapper.Models;
using Sapper.SapperEngine;

namespace Sapper
{
    public static class Extentions
    {
        public static SapperEngineModel GetModel(this SapperEngine.SapperEngine self)
        {
            var matrix = new string[self.Height][];

            for (int pi = 0; pi < self.Height; pi++)
            {
                matrix[pi] = new string[self.Width];
                for (int pj = 0; pj < self.Width; pj++)
                {
                    if (!self.Matrix[pj, pi])
                    {
                        matrix[pi][pj] = SapperEngineModel.CellTypeNames[SapperModelCellType.None];
                    }
                    else
                    {
                        matrix[pi][pj] = SapperEngineModel.CellTypeNames[SapperModelCellType.Hide];
                    }
                }
            }

            var model = new SapperEngineModel
            {
                Matrix = matrix,
                Width = self.Width,
                Height = self.Height,
                MinesNumber = self.MinesNumber,
                GameTimeMilliseconds = Math.Floor(self.GameTime.TotalMilliseconds).ToString()
            };

            return model;
        }

        private static long ConvertDateTimeToJavaScriptDateTicks(DateTime dt)
        {
            return (dt.Ticks - new DateTime(1970, 1, 1, 3, 0, 0).Ticks)/10000;
        }
    }
}