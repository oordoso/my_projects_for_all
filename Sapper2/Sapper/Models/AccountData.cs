﻿using System.ComponentModel.DataAnnotations;

namespace Sapper.Models
{
    public class AccountData
    {
        public int Id { get; set; }

        [Required]
        [RegularExpression(@"^[\w\ \.\-_]{1,50}[a-z]", ErrorMessage = "Use simple notation for name (length 1-50)")]
        public string Name { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Login range must be 6-20", MinimumLength = 6)]
        public string Login { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Password range must be 6-20", MinimumLength = 6)]
        [RegularExpression(
            ".*([a-z]+[A-Z]+[0-9]+|[a-z]+[0-9]+[A-Z]+|[0-9]+[a-z]+[A-Z]+|[0-9]+[A-Z]+[a-z]+|[A-Z]+[0-9]+[a-z]+|[A-Z]+[a-z]+[0-9]+).*",
            ErrorMessage = "Password must has 1 upper letter, 1 lower letter and 1 number")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Password is not confired")]
        public string Confirm { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Mail range must be lower then 30")]
        [RegularExpression("[a-z.]+@[a-z]+\\.[a-z]+", ErrorMessage = "Invalid e-mail")]
        public string Mail { get; set; }
    }
}