﻿using Sapper.JsonModels;

namespace Sapper.Models
{
    public class CheckoutData
    {
        public string Type { get; set; }
        public bool Hidden { get; set; }
        public bool Flag { get; set; }
        public int Value { get; set; }

        public int Estimate()
        {
            int estimate = 1;
            if (!Hidden)
                estimate += 5 + Value;

            if (Type == SapperEngineModel.CellTypeNames[SapperModelCellType.Mine] &&
                Flag)
                estimate += 20;

            return estimate;
        }
    }
}