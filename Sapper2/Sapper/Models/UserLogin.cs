﻿namespace Sapper.Models
{
    public class UserLogin // : IValidatableObject
    {
        private readonly string ErrorMessage = "Invalid Login.";

        public string Login { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }

        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    yield return new ValidationResult(ErrorMessage, new[] { "Error" });
        //}
    }
}