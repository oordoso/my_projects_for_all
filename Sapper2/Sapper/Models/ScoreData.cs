﻿using System;

namespace Sapper.Models
{
    public class ScoreData
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int MinesCount { get; set; }
        public string Type { get; set; }
        public DateTime StartTime { get; set; }
        public TimeSpan Time { get; set; }
        public int Score { get; set; }
    }
}