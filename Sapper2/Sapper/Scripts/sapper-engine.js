﻿function SapperHelper() {
    var self = this;

    self.createList = function() {
        var array = [];
        array.add = function(obj) {
            this.push(obj);
        };
        array.removeAt = function(index) {
            this.splice(index, 1);
        };
        return array;
    };
    self.createRandom = function() {
        return {
            next: function(minmax, max) {
                if (minmax == null)
                    return Math.random();
                else {
                    if (max == null)
                        return Math.random() * minmax;
                    else {
                        return minmax + (max - minmax) * Math.random();
                    }
                }
            },

            nextInt: function(minmax, max) {
                var rnd = this.next(minmax, max);
                return Math.floor(rnd);
            }
        };
    };
}

function SapperEngine(gameData) {
    var self = this;

    var cellTypes = {
        flag: "l",
        hide: "h", //corresponding to the server value
        mine: "m",
        free: "f",
        near: "e",
        none: "n" //corresponding to the server value
    };

    var gameStates = {
        inGame: "ingame",
        gameOver: "gameover",
        victory: "victory",
    };

    var width = gameData.Width;
    var height = gameData.Height;
    var minesNumber = gameData.MinesNumber;
    var avaliableFlagNumber = minesNumber;
    var state = gameStates.inGame;

    var matrix = [];

    var helper = new SapperHelper();

    //css style classes .sapper-cell.*
    var cellClass = {};
    cellClass[cellTypes.flag] = "flag";
    cellClass[cellTypes.hide] = "hide";
    cellClass[cellTypes.mine] = "mine";
    cellClass[cellTypes.free] = "free";
    cellClass[cellTypes.near] = "free";
    cellClass[cellTypes.none] = "none";

    //make object matrix
    var init = function() {
        for (var i = 0; i < height; i++) {
            matrix[i] = [];
            for (var j = 0; j < width; j++) {
                matrix[i][j] = {
                    column: j,
                    row: i,
                    type: gameData.Matrix[i][j], //inner hidden type
                    value: 0,
                    hidden: true,
                    flag: false,

                    //coloring representation of cells
                    getClass: function() {
                        return (this.isHidden()) ?
                            ((this.flag) ?
                                cellClass[cellTypes.flag] :
                                cellClass[cellTypes.hide]) :
                            cellClass[this.type];
                    },

                    //value representation
                    getValue: function() {
                        return (this.value && !this.isHidden()) ? this.value : " ";
                    },

                    open: function() {
                        this.hidden = false;
                        this.flag = false;
                    },

                    toggle: function() {
                        this.flag = !this.flag && this.hidden;
                    },

                    isHidden: function() {
                        return this.hidden && !this.isNone();
                    },

                    isNone: function() {
                        return this.type == cellTypes.none;
                    },

                    isMine: function() {
                        return this.type == cellTypes.mine;
                    },

                    isFree: function() {
                        return this.type == cellTypes.free;
                    },

                    isFlaged: function() {
                        return this.flag;
                    },
                };
            }
        }

    };

    //select mines from object matrix and recalculate values
    var generate = function() {
        var rand = helper.createRandom();
        var availablePoints = helper.createList();
        var minePoints = helper.createList();

        //generate all avaliable points as array of points
        for (var i = 0; i < height; i++) //*j - column, *i - row
            for (var j = 0; j < width; j++)
                if (isInMatrix(i, j))
                    availablePoints.add({
                        column: j,
                        row: i
                    });

        minesNumber = Math.min(minesNumber, availablePoints.length);

        //select mine points from avaliables
        for (var i = 0; i < minesNumber; i++) {
            var rndPos = rand.nextInt(availablePoints.length);
            var point = availablePoints[rndPos];
            availablePoints.removeAt(rndPos);
            minePoints.add(point);

            matrix[point.row][point.column].type = cellTypes.mine;
        }

        //calculate the value of around mine points
        for (var i = 0; i < minesNumber; i++) {
            var point = minePoints[i];
            for (var pi = point.row - 1; pi <= point.row + 1; pi++) //*j - column, *i - row
                for (var pj = point.column - 1; pj <= point.column + 1; pj++)
                    if (isInMatrix(pi, pj) && (pj != point.column || pi != point.row) && !matrix[pi][pj].isMine())
                        matrix[pi][pj].value++;
        }

        //change the type of availablePoints
        for (var i = 0; i < availablePoints.length; i++) {
            var point = availablePoints[i];
            if (matrix[point.row][point.column].value > 0)
                matrix[point.row][point.column].type = cellTypes.near;
            else
                matrix[point.row][point.column].type = cellTypes.free;
        }
    };

    var isInMatrix = function(row, column) {
        return 0 <= column && column < width &&
            0 <= row && row < height &&
            !matrix[row][column].isNone();
    };

    var openFreeCells = function(row, column) {
        //if no one mine exist near the cell
        //then open nearest with recursion

        if (!isInMatrix(row, column) || !matrix[row][column].isFree)
            return;

        //BFS-------------------------------------
        var listOpenedPoints = helper.createList();
        listOpenedPoints.add({
            column: column,
            row: row
        });

        while (listOpenedPoints.length > 0) {
            //*j - column, *i - row
            var cj = listOpenedPoints[0].column;
            var ci = listOpenedPoints[0].row;

            matrix[ci][cj].open();

            for (var i = 0; i < listOpenedPoints.length; i++) {
                var point = listOpenedPoints[i];
                if (!matrix[point.row][point.column].isHidden()) {
                    listOpenedPoints.removeAt(i);
                    i--;
                }
            }

            for (var pi = ci - 1; pi <= ci + 1; pi++)
                for (var pj = cj - 1; pj <= cj + 1; pj++)
                    if (isInMatrix(pi, pj)) {
                        if (matrix[pi][pj].isFree()) {
                            if (matrix[pi][pj].isHidden())
                                listOpenedPoints.add({
                                    column: pj,
                                    row: pi
                                });
                        } else
                            matrix[pi][pj].open();
                    }
        }
        //----------------------------------------
    };

    var isVictory = function() {
        //are all hidden cells only the mines
        for (var pi = 0; pi < height; pi++) //*j - column, *i - row
            for (var pj = 0; pj < width; pj++)
                if (!matrix[pi][pj].isNone() &&
                    matrix[pi][pj].isHidden() &&
                    !matrix[pi][pj].isMine())
                    return false;
        return true;
    };

    var openAllMines = function() {
        //is shown when you fall
        for (var pi = 0; pi < height; pi++) //*j - column, *i - row
            for (var pj = 0; pj < width; pj++)
                if (!matrix[pi][pj].isNone() && matrix[pi][pj].isMine())
                    matrix[pi][pj].open();
    };

    self.getViewMatrix = function() {
        //for angular rendering
        return matrix;
    };

    self.openCell = function(row, column) {
        //user selection

        if (!isInMatrix(row, column) || self.isGameEnd())
            return;

        if (matrix[row][column].isFree())
            openFreeCells(row, column);

        matrix[row][column].open();

        //check game winning condition
        if (matrix[row][column].isMine()) {
            state = gameStates.gameOver;
            openAllMines();
            return;
        }

        if (isVictory()) {
            state = gameStates.victory;
            return;
        }
    };

    self.openCellAround = function(row, column) {
        //open nearest and this one excepting the flag

        if (self.isGameEnd())
            return;

        for (var pi = row - 1; pi <= row + 1; pi++) //*j - column, *i - row
            for (var pj = column - 1; pj <= column + 1; pj++)
                if (isInMatrix(pi, pj) && !matrix[pi][pj].isFlaged())
                    self.openCell(pi, pj);
    };

    self.toggleFlag = function(row, column) {
        if (!isInMatrix(row, column) ||
            !matrix[row][column].isHidden() ||
            self.isGameEnd())
            return;

        if (matrix[row][column].isFlaged()) {
            matrix[row][column].toggle();
            avaliableFlagNumber++;
        } else {
            if (avaliableFlagNumber > 0) {
                matrix[row][column].toggle();
                avaliableFlagNumber--;
            }
        }
    };

    self.forceEndGame = function() {
        //when time is over

        openAllMines();
        state = gameStates.gameOver;
    };

    self.isGameEnd = function() {
        return state != gameStates.inGame;
    };

    self.isGameResultVictory = function() {
        return state == gameStates.victory;
    };

    self.getFlagsNumber = function() {
        return avaliableFlagNumber;
    };

    init();
    generate();
}