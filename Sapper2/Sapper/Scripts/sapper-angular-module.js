﻿(function() {
    var message = {
        loading: "Loading...",
        victory: "You win!!!",
        gameOver: "You lose...",
        avaliableFlags: "You have {0} flags",
        remainedTime: "Remaining time: {0}.",
        elapsedTime: "Elapsed time: {0}",
        score: "Score: {0}"
    };
    var getTimeDifference = function(earlyDate, laterDate) {
        var total = function(time) {
            if (typeof (time) == "object")
                return time.getTime();
            else
                return +time;
        };
        return Math.max(total(laterDate) - total(earlyDate), 0);
    };

    var stringFormat = function() {
        var s = arguments[0];
        for (var i = 0; i < arguments.length - 1; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, arguments[i + 1]);
        }
        return s;
    };

    var timeFormat = function(time) {
        var str = function(val) {
            if (val < 10)
                return "0" + val;
            return val;
        };
        var oDiff = {};
        var msec = time;
        oDiff.total = time;
        oDiff.h = Math.floor(msec / 1000 / 60 / 60);
        msec -= oDiff.h * 1000 * 60 * 60;
        oDiff.m = Math.floor(msec / 1000 / 60);
        msec -= oDiff.m * 1000 * 60;
        oDiff.s = Math.floor(msec / 1000);
        msec -= oDiff.s * 1000;
        oDiff.ms = msec;

        return str(oDiff.h) + ":" + str(oDiff.m) + ":" + str(oDiff.s) + "." + oDiff.ms;
    };
    var app = angular.module("sapper", []);

    app.controller("GameController", function($scope, $http, $interval) {
        var gameData = null;
        var game = null;
        var intervalPromise = null;

        var endTime = 0;
        var startTime = 0;

        $scope.viewMatrix = null;
        $scope.endTime = "";
        $scope.elapsedTime = "";
        $scope.score = "";

        //does show the table
        $scope.isGameInit = function() {
            return gameData != null;
        };

        //first run or Try again click
        $scope.createNewGame = function() {
            game = new SapperEngine(gameData);

            startTime = Date.now();
            endTime = startTime + +gameData.GameTimeMilliseconds;

            $scope.viewMatrix = game.getViewMatrix();
            $http.post("/Game/StartGame");

            startUpdating();
        };

        //user click
        $scope.onCellClick = function(cell, $event) {
            if ($event.altKey)
                game.openCellAround(cell.row, cell.column);
            else {
                if ($event.ctrlKey) {
                    game.toggleFlag(cell.row, cell.column);
                } else {
                    game.openCell(cell.row, cell.column);
                }
            }
        };

        //message about game state
        $scope.getCurrentMessage = function() {
            if (game == null)
                return message.loading;

            switch ($scope.getGameState()) {
            case 0:
                return stringFormat(message.avaliableFlags, game.getFlagsNumber());
            case 1:
                return message.victory;
            case -1:
                return message.gameOver;
            }

            return "";
        };

        //for rendering conditions
        $scope.getGameState = function() {
            if (game == null)
                return 0;

            if (game.isGameEnd())
                if (game.isGameResultVictory())
                    return 1;
                else
                    return -1;
            return 0;
        };

        //get cell matrix with selected figure
        var requestGameDataAndInit = function() {
            var responsePromise = $http.post("/Game/GameData");

            responsePromise.success(function(data) {
                gameData = data;
                $scope.createNewGame();
            });
        };

        //when the game is finished
        var requestScoreAndSaveResult = function() {
            var responsePromise = $http.post("/Game/Checkout", makeCheckoutData($scope.viewMatrix));

            responsePromise.success(function(data) {
                $scope.score = stringFormat(message.score, data);
            });
        };

        //select only useful info from view matrix
        var makeCheckoutData = function(viewMatrix) {
            var matrix = [];
            for (var row in viewMatrix) {
                var newrow = [];
                for (var column in viewMatrix[row]) {
                    var cell = viewMatrix[row][column];
                    newrow.push({
                        Type: cell.type,
                        Hidden: cell.hidden,
                        Flag: cell.flag,
                        Value: cell.value
                    });
                }
                matrix.push(newrow);
            }
            return matrix;
        };

        var updateTimers = function(remainedTime, elapsedTime) {
            $scope.endTime = stringFormat(
                        message.remainedTime,
                        remainedTime);
            $scope.elapsedTime = stringFormat(
                message.elapsedTime,
                elapsedTime);
        }

        //timers updater
        var startUpdating = function() {
            if (intervalPromise) {
                $interval.cancel(intervalPromise);
                intervalPromise = null;
            }

            intervalPromise = $interval(function() {
                if (game.isGameEnd()) {
                    $scope.score = stringFormat(message.score, "...");
                    if (game.isGameResultVictory())
                        requestScoreAndSaveResult();
                    else
                        $scope.score = stringFormat(message.score, 0);

                    $interval.cancel(intervalPromise);
                    intervalPromise = null;
                    return;
                }

                if (getTimeDifference(Date.now(), endTime) == 0) {
                    game.forceEndGame();
                    updateTimers(timeFormat(0), timeFormat(+gameData.GameTimeMilliseconds));
                    return;
                }

                var oneMonth = 25 * 24 * 60 * 60 * 1000;

                var remainedTime = getTimeDifference(Date.now(), endTime);
                var remainedTimeFormated = timeFormat(remainedTime);
                if (remainedTime > oneMonth)
                    remainedTimeFormated = "unlimeted";

                updateTimers(remainedTimeFormated, timeFormat(getTimeDifference(startTime, Date.now())));
            }, 37);
        };

        requestGameDataAndInit();
    });
})();