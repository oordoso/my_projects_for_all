﻿using System;
using System.Collections.Generic;
using Sapper.Models;

namespace Sapper.DatabaseEngine
{
    public partial class DataManager
    {
        public void Add(ScoreData data)
        {
            string sql = String.Format(
                SqlInsertIntoScore,
                data.AccountId,
                data.Width,
                data.Height,
                data.MinesCount,
                data.Type,
                data.StartTime.ToString("yyyy-MM-dd HH:mm:ss"),
                data.Time.Ticks,
                data.Score);

            ExecuteQuery(sql);
        }

        public ScoreData[] GetScoresByAccount(int accountId)
        {
            var list = new List<ScoreData>();

            string sql = String.Format(SqlSelectFromScore, "account_id=" + accountId);

            ExecuteQuery(sql, reader =>
            {
                while (reader.Read())
                    list.Add(new ScoreData
                    {
                        Id = (int) reader[0],
                        AccountId = (int) reader[1],
                        Width = (int) reader[2],
                        Height = (int) reader[3],
                        MinesCount = (int) reader[4],
                        Type = (string) reader[5],
                        StartTime = (DateTime) reader[6],
                        Time = new TimeSpan((long) reader[7]),
                        Score = (int) reader[8]
                    });
            });

            return list.ToArray();
        }
    }
}