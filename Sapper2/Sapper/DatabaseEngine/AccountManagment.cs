﻿using System;
using System.IO;
using Sapper.Models;

namespace Sapper.DatabaseEngine
{
    public partial class DataManager
    {
        public void Add(AccountData data)
        {
            if (IsExistByCondition("login", "login='" + data.Login + "'", "account"))
                throw new InvalidDataException("Account exist");

            string sql = String.Format(
                SqlInsertIntoAccount,
                data.Name,
                data.Login,
                data.Password,
                data.Mail);

            ExecuteQuery(sql);
        }

        public void Update(AccountData data)
        {
            if (!IsExistByCondition("id", "id=" + data.Id, "account"))
                throw new InvalidDataException("Account does not exist");

            string sql = String.Format(
                SqlUpdateAccount,
                data.Id,
                data.Name,
                data.Login,
                data.Password,
                data.Mail);

            ExecuteQuery(sql);
        }

        public void DeleteAccount(int id)
        {
            string sql = String.Format(SqlDeleteFromAccount, id);
            ExecuteQuery(sql);
        }

        private AccountData GetAccountByCondition(string condition)
        {
            AccountData account = null;

            string sql = String.Format(SqlSelectFromAccount, condition);

            ExecuteQuery(sql, reader =>
            {
                if (reader.Read())
                    account = new AccountData
                    {
                        Id = (int) reader[0],
                        Name = (string) reader[1],
                        Login = (string) reader[2],
                        Password = "",
                        Mail = (string) reader[3]
                    };
            });

            return account;
        }

        public AccountData GetAccount(int id)
        {
            return GetAccountByCondition("id=" + id);
        }

        public AccountData GetAccount(UserLogin userLogin)
        {
            return GetAccountByCondition(String.Format(SqlLoginAndPass, userLogin.Login, userLogin.Password));
        }

        public AccountData GetAccount(string login)
        {
            return GetAccountByCondition("login='" + login + "'");
        }

        public bool IsUserLoginValid(UserLogin userLogin)
        {
            return IsExistByCondition(
                "login, pass",
                String.Format(SqlLoginAndPass, userLogin.Login, userLogin.Password),
                "account");
        }
    }
}