﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace Sapper.DatabaseEngine
{
    public partial class DataManager
    {
        private readonly string _connectionString =
            ConfigurationManager.ConnectionStrings["DataConnection"].ConnectionString;

        private void ExecuteQuery(string sqlQuery, Action<SqlDataReader> executionFunc = null)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();

                if (executionFunc != null)
                    executionFunc(reader);
            }
        }

        private bool IsExistByCondition(string columnName, string condition, string tableName)
        {
            string sql = String.Format("select {0} from {2} where {1};", columnName, condition, tableName);
            bool result = false;
            ExecuteQuery(sql, reader => { result = reader.HasRows; });
            return result;
        }
    }
}