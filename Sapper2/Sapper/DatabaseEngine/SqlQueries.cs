﻿namespace Sapper.DatabaseEngine
{
    public partial class DataManager
    {
        private const string SqlInsertIntoAccount = @"insert into account (name, login, pass, mail)
values ('{0}','{1}','{2}','{3}');";
        private const string SqlUpdateAccount = @"update account set name='{1}', login='{2}', pass='{3}', mail='{4}')
where id={0};";
        private const string SqlDeleteFromAccount = @"delete from score
where account_id={0};
delete from account
where id={0};";
        private const string SqlSelectFromAccount = @"select id, name, login, mail
from account
where {0};";
        private const string SqlLoginAndPass = "login='{0}' and pass='{1}'";

        private const string SqlInsertIntoScore =
            @"insert into score (account_id, width, height, mine, type, start_time, time, score)
values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}');";

        private const string SqlSelectFromScore =
            @"select id, account_id, width, height, mine, type, start_time, time, score
from score
where {0};";
    }
}