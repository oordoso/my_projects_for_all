﻿using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using Sapper.DatabaseEngine;
using Sapper.Helpers;
using Sapper.JsonModels;
using Sapper.Models;

namespace Sapper.Controllers
{
    [Authorize]
    public class GameController : Controller
    {
        private DataManager Repository
        {
            get
            {
                if (Session["repository"] == null)
                    Session.Add("repository", new DataManager());
                return Session["repository"] as DataManager;
            }
        }

        private AuthorizationHelper Helper
        {
            get
            {
                if (Session["helper"] == null)
                    Session.Add("helper", new AuthorizationHelper());
                var helper = Session["helper"] as AuthorizationHelper;
                helper.HttpContext = HttpContext;
                helper.Repository = Repository;
                return helper;
            }
        }

        private AccountData CurrentUser
        {
            get
            {
                return Helper.CurrentUser;
            }
        }

        private SapperEngine.SapperEngine Game
        {
            get { return (SapperEngine.SapperEngine) Session["game"]; }
            set { Session.Add("game", value); }
        }

        private GameSettings Settings
        {
            get { return (GameSettings) Session["settings"]; }
            set { Session.Add("settings", value); }
        }

        //
        // GET: /Game/

        public ActionResult Index()
        {
            if (Game == null)
                return RedirectToAction("Create");

            return View();
        }

        //
        // GET: /Game/Create

        public ActionResult Create()
        {
            return View(new GameSettings());
        }

        public ActionResult Reload()
        {
            if (Settings == null)
                return RedirectToAction("Create");

            var settingEndTime = new TimeSpan(31,0,0,0);
            if (Settings.IsTimeLimit)
                settingEndTime = Settings.TimeLimit;

            Game = new SapperEngine.SapperEngine(
                Settings.Width,
                Settings.Height,
                Settings.MineNumber,
                settingEndTime,
                Settings.SelectedFigure);

            return RedirectToAction("Index");
        }

        //
        // POST: /Game/Create

        [HttpPost]
        public ActionResult Create(GameSettings settings)
        {
            if (ModelState.IsValid)
            {
                Settings = settings;
                return RedirectToAction("Reload");
            }
            return View(settings);
        }

        public ActionResult Close()
        {
            Game = null;
            return RedirectToAction("Create");
        }
        
        [HttpPost]
        public void GameData()
        {
            SapperEngineModel model = Game.GetModel();
            Response.Write(JsonConvert.SerializeObject(model));
        }

        

        [HttpPost]
        public void StartGame()
        {
            Game.StartGame();
        }

        [HttpPost]
        public int Checkout(CheckoutData[][] resolvedGameMatrix)
        {
            Game.StopGame();
            if (!Game.IsValid())
                return 0;

            double estimate = Game.Estimate(resolvedGameMatrix);

            var score = new ScoreData()
            {
                AccountId = CurrentUser.Id,
                Width = Game.Width,
                Height = Game.Height,
                MinesCount = Game.MinesNumber,
                Type = Settings.SelectedFigureName,
                StartTime = Game.StartTime,
                Time = Game.ElapsedTime,
                Score = (int)estimate
            };
            Repository.Add(score);
            
            return (int)estimate;
        }
    }
}