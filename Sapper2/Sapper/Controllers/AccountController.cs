﻿using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Sapper.DatabaseEngine;
using Sapper.Helpers;
using Sapper.Models;

namespace Sapper.Controllers
{
    public class AccountController : Controller
    {
        private DataManager Repository
        {
            get
            {
                if (Session["repository"] == null)
                    Session.Add("repository", new DataManager());
                return Session["repository"] as DataManager;
            }
        }

        private AuthorizationHelper Helper 
        {
            get
            {
                if (Session["helper"] == null)
                    Session.Add("helper", new AuthorizationHelper());
                var helper = Session["helper"] as AuthorizationHelper;
                helper.HttpContext = HttpContext;
                helper.Repository = Repository;
                return helper;
            }
        }

        private bool IsAutorized
        {
            get
            {
                return Helper.IsAuthorized;
            }
        }

        private AccountData CurrentUser
        {
            get
            {
                return Helper.CurrentUser;
            }
        }

        public ActionResult Login()
        {
            if (IsAutorized)
                return Redirect("/");
            return View(new UserLogin());
        }

        [HttpPost]
        public ActionResult Login(UserLogin model, string returnUrl)
        {
            if (Helper.Login(model))
                return RedirectToLocal(returnUrl);

            model.Login = "";
            model.Password = "";
            ModelState.AddModelError("Error", "Invalid login or password.");
            return View(model);
        }

        public ActionResult SignOut()
        {
            Helper.LogOut();
            return Redirect("/");
        }

        [Authorize]
        public ActionResult UserProfile()
        {
            return View(CurrentUser);
        }

        //
        // GET: /Acount/Create

        public ActionResult Registration()
        {
            return View(new AccountData());
        }

        //
        // POST: /Acount/Create

        [HttpPost]
        public ActionResult Registration(AccountData userData)
        {
            if (ModelState.IsValid)
            {
                if (Repository.GetAccount(userData.Login) == null)
                {
                    Repository.Add(userData);
                    return RedirectToAction("Login");
                }

                ModelState.AddModelError("Login", "Login already exist");
            }

            userData.Password = "";
            userData.Confirm = "";
            return View(userData);
        }

        [Authorize]
        public ActionResult Delete()
        {

            Repository.DeleteAccount(CurrentUser.Id);
            return SignOut();
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}