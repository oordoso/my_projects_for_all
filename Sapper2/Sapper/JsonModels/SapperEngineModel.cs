﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Sapper.SapperEngine;

namespace Sapper.JsonModels
{
    public class SapperEngineModel
    {
        [JsonIgnore] public static readonly Dictionary<SapperModelCellType, string> CellTypeNames = new Dictionary
            <SapperModelCellType, string>
        {
            {SapperModelCellType.Flag, "l"},
            {SapperModelCellType.Hide, "h"},
            {SapperModelCellType.Mine, "m"},
            {SapperModelCellType.Free, "f"},
            {SapperModelCellType.None, "n"}
        };

        public string[][] Matrix { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int MinesNumber { get; set; }
        public string GameTimeMilliseconds { get; set; }
    }
}