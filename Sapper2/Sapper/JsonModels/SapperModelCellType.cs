﻿namespace Sapper.JsonModels
{
    public enum SapperModelCellType
    {
        Flag,
        Hide,
        Mine,
        Free,
        None
    }
}