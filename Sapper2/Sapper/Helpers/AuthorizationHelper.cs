﻿using System;
using System.Web;
using System.Web.Security;
using Sapper.DatabaseEngine;
using Sapper.Models;

namespace Sapper.Helpers
{
    public class AuthorizationHelper
    {
        private const string CookieName = "__AUTH_COOKIE";

        private AccountData _currentUser;
        private bool _isAuthorized;

        public HttpContextBase HttpContext { get; set; }

        public DataManager Repository { get; set; }

        public AccountData CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    AuthorizationFromCookie();
                }
                return _currentUser;
            }
        }

        public bool IsAuthorized
        {
            get
            {
                if (_currentUser == null)
                {
                    AuthorizationFromCookie();
                }
                return _isAuthorized;
            }
        }

        private void AuthorizationFromCookie()
        {
            try
            {
                HttpCookie authCookie = HttpContext.Request.Cookies.Get(CookieName);
                if (authCookie != null && !string.IsNullOrEmpty(authCookie.Value))
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    FormsAuthentication.SetAuthCookie(ticket.Name, false);
                    _currentUser = Repository.GetAccount(ticket.Name);
                    _isAuthorized = true;
                }
                else
                {
                    _currentUser = new AccountData();
                    _isAuthorized = false;
                }
            }
            catch (Exception ex)
            {
                _currentUser = new AccountData();
                _isAuthorized = false;
            }
        }

        public bool Login(UserLogin userLogin)
        {
            bool isValid = Repository.IsUserLoginValid(userLogin);
            if (isValid)
            {
                ResetAuthorizationFields();
                CreateCookie(userLogin.Login, userLogin.RememberMe);
            }
            return isValid;
        }

        private void CreateCookie(string login, bool isPersistent = false)
        {
            var ticket = new FormsAuthenticationTicket(
                1,
                login,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                isPersistent,
                string.Empty,
                FormsAuthentication.FormsCookiePath);

            // Encrypt the ticket.
            string encTicket = FormsAuthentication.Encrypt(ticket);

            // Create the cookie.
            var AuthCookie = new HttpCookie(CookieName)
            {
                Value = encTicket,
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };

            FormsAuthentication.SetAuthCookie(ticket.Name, isPersistent);
            HttpContext.Response.Cookies.Set(AuthCookie);
        }

        private void ResetAuthorizationFields()
        {
            _currentUser = null;
        }

        public void LogOut()
        {
            ResetAuthorizationFields();
            FormsAuthentication.SignOut();
            HttpCookie httpCookie = HttpContext.Response.Cookies[CookieName];
            if (httpCookie != null)
            {
                httpCookie.Value = string.Empty;
            }
        }
    }
}